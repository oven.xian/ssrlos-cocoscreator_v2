var KeyboardSimulator = cc.Class({
    extends: cc.Component,

    properties: {
    },

    onLoad:function () {
        this.allKeys = {};
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    },

    onDestroy:function () {
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    },

    onKeyDown:function (event) {
        this.allKeys[event.keyCode] = true;
    },

    onKeyUp:function (event) {
        this.allKeys[event.keyCode] = false;
    },
});