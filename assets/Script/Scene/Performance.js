//
const ssr = require('../line-of-sight/namespace/SSRLoSNamespace');

cc.Class({
    extends: cc.Component,

    properties: {
    	robot: {
    		default: null,
    		type: cc.Node
    	},
    	obstaclesGroup: {
            default         : null,
            type            : cc.Node
        },
        lightsGroup: {
            default         : null,
            type            : cc.Node
        },
        hexagonPrefab: {
            default         : null,
            type            : cc.Prefab  
        },
        lightPrefab: {
            default         : null,
            type            : cc.Prefab  
        },
    },

    start: function () {
        cc.debug.setDisplayStats(true);
        // ssr.LoS.Config.__initModules();
        this._obstacles = [];
        this._lightSources = [];
        this.camera = cc.find("Main Camera", this.node).getComponent(cc.Camera);
        this.robotObject = this.robot.getComponent("Robot");
        this.robotLoSComponent = this.robot.getComponent("SSRLoSComponentCore");
        this.robotLoSCore = this.robotLoSComponent.getLoSCore();
        this.randomObstacles(10);

    },
    randomObstacles:function(count) {
        this.obstaclesGroup.removeAllChildren(true);
        this._obstacles = [];
        this.robotLoSCore.removeAllObstacles();
        //
        var w = cc.winSize.width;
        var h = cc.winSize.height;
        var size = (count > 100 ? 10 : 12);
        var wCount = count / 10;
        var hCount = 10;
        this._obstacles = [];
        for (var i = 0; i < wCount; i ++) {
            for (var j = 0; j < hCount; j ++) {
                var hexagonNode = cc.instantiate(this.hexagonPrefab);
                hexagonNode.parent = this.obstaclesGroup;
                hexagonNode.setPosition(-w / 2 + size + w / (wCount + 1) * (i + 1), -h / 2 + 10 + size + (h - size) / hCount * j);
                this._obstacles.push(hexagonNode);
                var hexagonObstacle = hexagonNode.getComponent("PolygonColliderObstacle");
                var obstalce = this.robotLoSCore.addObstacle(cc.sys.isNative ? hexagonNode._proxy : hexagonObstacle, hexagonObstacle.getVertexArray());
                obstalce.setVertexArrayProvider(hexagonObstacle.getVertexArray, hexagonObstacle);
            }
        }
        for (var i = 0; i < this._lightSources.length; i ++) {
            this._lightSources[i].getComponent("SSRLoSComponentCore").getLoSCore().removeAllObstacles();
            for (var o = 0; o < this._obstacles.length; o ++) {
                var hexagonObstacle = this._obstacles[o].getComponent("PolygonColliderObstacle");
                var obstalce = this._lightSources[i].getComponent("SSRLoSComponentCore").getLoSCore().addObstacle(cc.sys.isNative ? this._obstacles[o]._proxy : hexagonObstacle, hexagonObstacle.getVertexArray());
                obstalce.setVertexArrayProvider(hexagonObstacle.getVertexArray, hexagonObstacle);
            }
        }
    },
    randomLights:function(count) {
        this.lightsGroup.removeAllChildren(true);
        this._lightSources = [];

        var w = cc.winSize.width;
        var h = cc.winSize.height;
        var size = 10;
        var hCount = 10;

        for (var i = 0; i < count; i ++) {
            var lightNode = cc.instantiate(this.lightPrefab);
            lightNode.parent = this.lightsGroup;
            lightNode.setPosition(-w / 2 + 20, -h / 2 + 10 + size + (h - size) / hCount * i + 30);
            lightNode.runAction(
                cc.repeatForever(
                    cc.sequence(
                        cc.moveBy(20 + i * 2, cc.v2(w - 10, 0)),
                        cc.moveBy(20 + i * 2, cc.v2(-w + 10, 0))
                    )
                )
            );
            this._lightSources.push(lightNode);
            for (var o = 0; o < this._obstacles.length; o ++) {
                var hexagonObstacle = this._obstacles[o].getComponent("PolygonColliderObstacle");
                var obstalce = lightNode.getComponent("SSRLoSComponentCore").getLoSCore().addObstacle(cc.sys.isNative ? this._obstacles[o]._proxy : hexagonObstacle, hexagonObstacle.getVertexArray());
                obstalce.setVertexArrayProvider(hexagonObstacle.getVertexArray, hexagonObstacle);
            }
        }
    },
    // called every frame
    update: function (dt) {
        if (this.robotObject.useCamera) {
            this.camera.node.position = this.robot.position;
        }
        if (this.robotObject.isForceLoSUpdate) {
            this.robotLoSCore.enableForceUpdate();
            // this.robotLoSCore.setDirtyFlag(ssr.LoS.Constant.DIRTY_FLAGS.BOUNDARY);
            // this.robotLoSCore.setDirtyFlag(ssr.LoS.Constant.DIRTY_FLAGS.CULLING);
        }
        //
        var isUpdated = this.robotLoSComponent.updateSightNode();
        if (isUpdated || this.robotObject.isForceLoSUpdate) {
            this.node.getComponent("LoSRenderLayer").updateRender(this.robotObject.isForceLoSUpdate);
            this.node.getComponent("FloatingMenu").updateDebugDraw();
        }
        this.robotLoSCore.disableForceUpdate();
        this.robotObject.isForceLoSUpdate = false;
        //
        for (var i = 0; i < this._lightSources.length; i ++) {
            isUpdated = this._lightSources[i].getComponent("SSRLoSComponentCore").updateSightNode();
            this._lightSources[i].getChildByName("SightLightRender").getComponent("SSRLoSComponentRender").plot();
        }
    }
});
