
var PolygonColliderObstacle = cc.Class({
    extends: cc.Component,
    editor: CC_EDITOR && {
        executeInEditMode: true
    },
    properties: {
    },
    onLoad:function () {
        this.render = this.node.getComponent("cc.Graphics");
        this.polygonCollider = this.node.getComponent(cc.PolygonCollider);
        this.plot();
    },
    plot:function() {
        this.render.clear();
        for (var i = 0; i < this.polygonCollider.points.length; i ++) {
            var pt = this.polygonCollider.points[i];
            if (i == 0) {
                this.render.moveTo(pt.x + this.polygonCollider.offset.x, pt.y + this.polygonCollider.offset.y);
            }  
            else {
                this.render.lineTo(pt.x + this.polygonCollider.offset.x, pt.y + this.polygonCollider.offset.y);
            }
        }
        this.render.fill();
    },
    updateVertexArray:function(points) {
        this.polygonCollider.points = points;
        this.plot();
    },
    getVertexArray:function() {
        var vertexArray = [];
        for (var j = 0; j < this.polygonCollider.points.length; j ++) {
            var point = this.polygonCollider.points[j];
            var point2 = point.rotate(this.node.angle * Math.PI / 180);
            vertexArray.push(cc.v2(point2.x + this.node.x + this.polygonCollider.offset.x, point2.y + this.node.y + this.polygonCollider.offset.y));
        }
        return vertexArray;
    },
    // CC_EDITOR
    _resetRender: CC_EDITOR && function () {
        this.plot();
    },
    onFocusInEditor: CC_EDITOR && function () {
        this.plot();
    },
    onLostFocusInEditor: CC_EDITOR && function () {
        this.plot();
    },
    onEnable: CC_EDITOR && function () {
        this.plot();
    },
    onDisable: CC_EDITOR && function () {
        this.plot();
    },
    lateUpdate: CC_EDITOR && function (delta) {
        cc.engine.repaintInEditMode();
        this.plot();
    },
});
