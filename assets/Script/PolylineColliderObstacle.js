
var PolylineColliderObstacle = cc.Class({
    extends: cc.Component,
    editor: CC_EDITOR && {
        executeInEditMode: true
    },
    properties: {
    },
    onLoad:function () {
        this.render = this.node.getComponent("cc.Graphics");
        this.polylineCollider = this.node.getComponent(cc.PhysicsChainCollider);
        this.plot();
    },
    plot:function() {
        this.render.clear();
        for (var i = 0; i < this.polylineCollider.points.length; i ++) {
            var pt = this.polylineCollider.points[i];
            if (i == 0) {
                this.render.moveTo(pt.x, pt.y);
            }  
            else {
                this.render.lineTo(pt.x, pt.y);
            }
        }
        this.render.stroke();
    },
    getVertexArray:function() {
        var vertexArray = [];
        for (var j = 0; j < this.polylineCollider.points.length; j ++) {
            vertexArray.push(cc.v2(this.polylineCollider.points[j].x + this.node.x, this.polylineCollider.points[j].y + this.node.y));
        }
        return vertexArray;
    },
    updateVertexArray:function(points) {
        this.polylineCollider.points = points;
        this.plot();
    },
    // CC_EDITOR
    _resetRender: CC_EDITOR && function () {
        this.plot();
    },
    onFocusInEditor: CC_EDITOR && function () {
        this.plot();
    },
    onLostFocusInEditor: CC_EDITOR && function () {
        this.plot();
    },
    onEnable: CC_EDITOR && function () {
        this.plot();
    },
    onDisable: CC_EDITOR && function () {
        this.plot();
    },
    lateUpdate: CC_EDITOR && function (delta) {
        cc.engine.repaintInEditMode();
        this.plot();
    },
});
