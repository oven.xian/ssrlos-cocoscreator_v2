## About / 关于

一句话介绍一下这个项目，就是基于 `cocos` 引擎的 `2d` 光线追踪，视野范围计算，渲染相关的东西。

![ccdungeons1](http://supersuraccoon.gitee.io/ssrlos-doc/res/ssrlos/ccdungeons1.png)

![ccdungeons2](http://supersuraccoon.gitee.io/ssrlos-doc/res/ssrlos/ccdungeons2.png)

![ccdungeons3](http://supersuraccoon.gitee.io/ssrlos-doc/res/ssrlos/ccdungeons3.png)



在我看来，截至到现在，这个项目的  `80%` 精力都是放在了`算法`的优化在优化上，剩下的 `20%` 精力则是在`渲染`上。项目陆陆续续的已经做了很久，但是还有很多很多想做的。随便写一几个:

- 已有通用算法的优化
- 定制型算法，针对一些特定使用场景的算法
- 纯 `GPU` 算法，半 `CPU` 半 `GPU` 算法
- 算法已经做了很多，要的数据也到手了，那当然在渲染上要花大力气优化，做出酷炫的效果了
- 更多的就写在文末的 `后续计划` 里了 ......

___

项目是从 `cocos2dx` 开始的，后续新功能的开发应该也会从 `cocos2dx` 入手，毕竟引擎稳定，我也是最熟悉。

目前几乎所有功能都已经移植了 `Creator v1, v2` 版本。

而且所有版本都做了 `Native Binding`。

但是因为精力有限，后面的新功能打算暂时只对应 `cocos2dx` 和 `creator v2`，当然 `creator v3` 出来的话也会考虑。暂时只对应 `Web` 版，当然原生也是可以直接用的，`Native Binding` 暂时不会去对应新功能。

**希望这个项目能做的长久** ......

___



## Resources / 资源

### Repository / 仓库

[SSRLoS-Cocos2dx v3.17](https://gitee.com/supersuraccoon/ssrlos-cocos2dx)

[SSRLoS-CocosCreator_v1 v1.10](https://gitee.com/supersuraccoon/ssrlos-cocoscreator_v1)

[SSRLoS-CocosCreator_v2 v2.40](https://gitee.com/supersuraccoon/ssrlos-cocoscreator_v2)

### Online Demo / 在线演示

[SSRLoS-Cocos2dx Demo](http://supersuraccoon.gitee.io/ssrlos-cocos2dx)

[SSRLoS-CocosCreator_v1 Demo](https://gitee.com/supersuraccoon/ssrlos-cocoscreator_v1)

[SSRLoS-CocosCreator_v2 Demo](http://supersuraccoon.gitee.io/ssrlos-cocoscreator_v2)

[SSRLoS-Cocos2dx API Demo](---)

#### codesandbox.io

基于 `cocos2d-x` 版本

[SSRLoS-Cocos2dx-Visibility](https://codesandbox.io/s/ssrlos-demo-visibility-744mo)

![demo](http://supersuraccoon.gitee.io/ssrlos-doc/res/api/visibility.png)

[SSRLoS-Cocos2dx-Shadow](https://codesandbox.io/s/ssrlos-demo-shadow-q44qo)

![demo](http://supersuraccoon.gitee.io/ssrlos-doc/res/api/shadow.png)

[SSRLoS-Cocos2dx-Modes](https://codesandbox.io/s/ssrlos-demo-modes-yxbgj)

![demo](http://supersuraccoon.gitee.io/ssrlos-doc/res/api/modes.png)

[SSRLoS-Cocos2dx-Dirty-Detection](https://codesandbox.io/s/ssrlos-demo-dirty-detection-eft0n)

![demo](http://supersuraccoon.gitee.io/ssrlos-doc/res/api/dirty_detection.png)

[SSRLoS-Cocos2dx-Light](https://codesandbox.io/s/ssrlos-demo-light-0h9gj)

![demo](http://supersuraccoon.gitee.io/ssrlos-doc/res/api/light.png)

[SSRLoS-Cocos2dx-Mask](https://codesandbox.io/s/ssrlos-demo-mask-gihmn)

![demo](http://supersuraccoon.gitee.io/ssrlos-doc/res/api/mask.png)

[SSRLoS-Cocos2dx-Multi-Lights](https://codesandbox.io/s/ssrlos-demo-multi-lights-m2ryx)

![demo](http://supersuraccoon.gitee.io/ssrlos-doc/res/api/multi_lights.png)

[SSRLoS-Cocos2dx-Multi-Masks](https://codesandbox.io/s/ssrlos-demo-multi-masks-f68bq)

![demo](http://supersuraccoon.gitee.io/ssrlos-doc/res/api/multi_masks.png)

[SSRLoS-Cocos2dx-Output-Render](https://codesandbox.io/s/ssrlos-demo-output-render-nwij2)

![demo](http://supersuraccoon.gitee.io/ssrlos-doc/res/api/output_render.png)

### Native Demo / 原生演示

creator v2.4.0 纯 js 版本

[v0.0.1_LoSCCC2_Web.apk](https://gitee.com/supersuraccoon/ssrlos-cocos2dx/attach_files/490534/download)

creator v1.10 纯 js 版本

[v0.0.1_LoSCCC1_Web.apk](https://gitee.com/supersuraccoon/ssrlos-cocos2dx/attach_files/490533/download)

cocos2dx v3.17 jsbinding 版本

[v0.0.1_LoSCC_Native.apk](https://gitee.com/supersuraccoon/ssrlos-cocos2dx/attach_files/490531/download)

cocos2dx v3.17 纯 js 实现版本，无 jsbinding 实现

[v0.0.1_LoSCC_Web.apk](https://gitee.com/supersuraccoon/ssrlos-cocos2dx/attach_files/490530/download)

### Doc / 文档

[API / 文档](https://gitee.com/supersuraccoon/ssrlos-doc)

[Wiki / 百科](https://gitee.com/supersuraccoon/ssrlos-doc/wikis/pages)

### Posts / 讨论帖子

[LoS - 视野, 光照, 阴影 实现分享 - forum.cocos.org](https://forum.cocos.org/t/los/98673)

### Plan / 计划

后续的计划，同时包括了很多 `2d` 的光照，阴影小关的技术文章，有兴趣可以看下。

[ To Explore / 待探索](https://gitee.com/supersuraccoon/ssrlos-doc/wikis/pages?sort_id=3006599&doc_id=1008958)